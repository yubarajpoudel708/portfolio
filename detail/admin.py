from django.contrib import admin

# Register your models here.
from detail.models import Blog, Author, Contact, Work, Education, AboutMe

class ContactAdmin(admin.ModelAdmin):
    list_dlist_displayisplay = ['email', 'full_name']
    list_filter = ['email']

class BlogAdmin(admin.ModelAdmin):
    list_display = ['author', 'title', 'description']

class EducationInline(admin.StackedInline):
    model = Education
    extra = 2

class AboutMeadmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'description']}),
        ('Social Link', {'fields': ['facebook_link', 'twitter_link', 'linkedin_link'], 'classes': ['collapse']}),
    ]
    inlines = [EducationInline]

admin.site.register(Blog, BlogAdmin)
admin.site.register(Author)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Work)
admin.site.register(AboutMe, AboutMeadmin)

