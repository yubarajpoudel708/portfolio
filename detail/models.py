from django.db import models

# Create your models here.
class Author(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    last_name = models.CharField(max_length=150, blank=False)
    email = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

class Blog(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True)
    image = models.FileField(upload_to="img", blank=True)
    published_date = models.DateTimeField(auto_now=True)
    publish = models.BooleanField(default=False)

    def __str__(self):
        return self.title

class AboutMe(models.Model):
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)
    facebook_link = models.CharField(max_length=100, blank=True)
    twitter_link = models.CharField(max_length=100, blank=True)
    linkedin_link = models.CharField(max_length=100, blank=True)

class Education(models.Model):
    aboutme = models.ForeignKey(AboutMe, on_delete=models.CASCADE)
    degree_type = (("SLC", "School Leaving certificate"), ("Bachelor", "Bachelor"), ("Master", "Master"))
    degree = models.CharField(max_length=50, choices=degree_type, default="SLC")
    percentage = models.IntegerField(default=0)
    passed_year = models.DateField()


class Work(models.Model):
    title = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)
    image = models.FileField(upload_to='work', blank=True)
    web_link = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.title

class Contact(models.Model):
    email = models.CharField(max_length=100, blank=True)
    full_name = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return self.full_name