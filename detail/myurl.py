from django.urls import path

from . import views

urlpatterns = [
    path('', views.blogList, name='bloglist'),
    path('aboutme/', views.aboutMe, name = "aboutme"),
    path('mywork/', views.myWork, name= "mywork"),
    path('contact/', views.addContact, name = "contact"),
    path("blog/<int:id>", views.blogDetail, name = "blogdetail"),
    path("contactform/", views.contactForm, name = "contactform"),

]