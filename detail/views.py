from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from detail.forms import ContactForm
from detail.models import Blog, Contact, Work


def index(request):
    return HttpResponse("hello this is detail index page")

def blogList(request):
    # need html template to show the bloglist
    # access the data from the model
    # pass the data to the bloglist html
    if request.method == "GET":
        data = Blog.objects.all()
        print(len(data))
        return render(request, 'detail/bloglist.html', {'bloglist':data})
    else:
        return HttpResponse("Only supports get request")

def aboutMe(request):
    return render(request, 'detail/aboutme.html')

def blogDetail(request, id):
    blog = Blog.objects.get(pk=id)
    return render(request, 'detail/blogdetail.html', {"blog": blog})

def addContact(request):
    if request.method == 'GET':
        return render(request, 'detail/contact.html')
    if request.method == 'POST':
        # get the data from the request body
        name = request.POST.get('fullname')
        email = request.POST.get('email')
        # save the data
        contact = Contact(full_name= name, email=email)
        contact.save()
        return HttpResponse("name = {}, email = {} is saved in server".format(name, email))

def myWork(request):
    # fetch all the work list from the table
    # pass the list to the template
    # retrieve
    workList = Work.objects.all()
    # pass the template
    return render(request, 'detail/mywork.html', {'data': workList})


def admin(request):
    return render(request, 'detail/admin.html')

def contactForm(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            def save(self):
                data = self.cleaned_data
                # contact = Contact(email=data['email'], first_name=data['first_name'],
                #             last_name=data['last_name'], password1=data['password1'],
                #             password2=data['password2'])
                # contact.save()
            pass  # does nothing, just trigger the validation
    else:
        form = ContactForm()
    return render(request, 'detail/contactform.html', {'form': form})